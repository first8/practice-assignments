import java.net.URISyntaxException;
import java.util.Collections;
import nl.first8.moj.webserver.server.ServerException;
import nl.first8.moj.webserver.server.WebServer;
import nl.first8.moj.webserver.server.WebServer.Host;
import org.junit.Test;
import static org.junit.Assert.*;

public class MalformedUrlTest {

    @Test
    public void testHandleMalformedUrl() throws ServerException {
        WebServer server = new WebServer(Host.localhost(80), Collections.emptySet());

        try {
            server.handle("http://localhost:8080</account>");
            fail("A URISyntaxException should have been thrown");
        } catch (URISyntaxException ex) {

        }
    }
}
