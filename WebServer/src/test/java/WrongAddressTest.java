import java.net.URISyntaxException;
import java.util.Collections;
import nl.first8.moj.webserver.server.ServerException;
import nl.first8.moj.webserver.server.WebServer;
import nl.first8.moj.webserver.server.WebServer.Host;
import org.junit.Test;
import static org.junit.Assert.*;

public class WrongAddressTest {

    @Test
    public void testHandleWrongAddress() throws URISyntaxException {

        WebServer server = new WebServer(Host.localhost(80), Collections.emptySet());

        try {
            server.handle("http://localhost:8080/account");
            fail("A ServerException should have been thrown");
        } catch (ServerException ex) {

        }
    }
}
