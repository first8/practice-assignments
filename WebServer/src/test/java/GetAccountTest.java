import static org.junit.Assert.fail;

import nl.first8.moj.webserver.account.Account;
import nl.first8.moj.webserver.account.AccountResource;
import nl.first8.moj.webserver.server.ServerException;
import nl.first8.moj.webserver.server.WebServer;
import org.junit.Test;
import java.net.URISyntaxException;
import java.util.Collections;

public class GetAccountTest {

    @Test
    public void testHandleGetAccount() throws ServerException {
        WebServer server = new WebServer(WebServer.Host.localhost(8080), Collections.singleton(AccountResource.class));

        try {
            Object result = server.handle("http://localhost:8080/account/1");
        } catch (URISyntaxException ex) {

        }
    }
}
