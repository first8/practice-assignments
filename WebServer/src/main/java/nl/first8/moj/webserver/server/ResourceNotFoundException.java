package nl.first8.moj.webserver.server;

//READ-ONLY
public final class ResourceNotFoundException extends RuntimeException {

    private final Class<?> requestedType;
    private final Object requestedId;

    public ResourceNotFoundException(Class<?> requestedObject, Object requestedId) {
        this.requestedType = requestedObject;
        this.requestedId = requestedId;
    }

    public Class<?> getRequestedType() {
        return requestedType;
    }

    public Object getRequestedId() {
        return requestedId;
    }

}
