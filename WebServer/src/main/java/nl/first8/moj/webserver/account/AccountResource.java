package nl.first8.moj.webserver.account;

import nl.first8.moj.webserver.api.Api;
import nl.first8.moj.webserver.server.ResourceNotFoundException;
import java.util.Arrays;
import java.util.List;

//READ-ONLY
@Api.Resource("account")
public final class AccountResource {

    private final List<Account> accounts = Arrays.asList(new Account(1, "Mark", "m.reinhold"));

    @Api.GET
    public Account get(@Api.Param("id") final int id) {
        throw new ResourceNotFoundException(Account.class, id);
    }
}
