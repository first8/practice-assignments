package nl.first8.moj.webserver.account;

//READONLY

public final class Account {

    private final int id;
    private final String userName;
    private final String emailAddress;

    Account(int id, String userName, String emailAddress) {
        this.id = id;
        this.userName = userName;
        this.emailAddress = emailAddress;
    }

    public int getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }
}
