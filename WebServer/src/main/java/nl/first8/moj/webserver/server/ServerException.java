package nl.first8.moj.webserver.server;

// READ-ONLY
public final class ServerException extends Exception {

    public ServerException(Throwable thrwbl) {
        super(thrwbl);
    }
}
