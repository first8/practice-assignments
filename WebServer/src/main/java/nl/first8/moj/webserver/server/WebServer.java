package nl.first8.moj.webserver.server;

import nl.first8.moj.webserver.api.Api;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;

//MODIFIABLE
public class WebServer {

    public static final class Host {

        private final String host;
        private final int port;

        private Host(String host, int port) {
            this.host = host;
            this.port = port;
        }

        public static Host of(final String host, final int port) {
            return new Host(host, port);
        }

        public static Host localhost(final int port) {
            return of("localhost", port);
        }

        public String getHost() {
            return host;
        }

        public int getPort() {
            return port;
        }
    }

    private final Host root;
    private final Set<Class<?>> resources;

    public WebServer(Host root, Set<Class<?>> resources) {
        this.root = root;
        this.resources = Collections.unmodifiableSet(new HashSet<>(resources));
    }

    public Object handle(String url) throws ServerException, URISyntaxException {

        //TODO -- solution
        final URI request = new URI(url);

        if (!root.getHost().equalsIgnoreCase(request.getHost()) || root.getPort() != request.getPort()) {
            throw new ServerException(new IllegalStateException());
        }

        final String requestedResource = request.getPath().split("/")[1];

        Class<?> resourceClazz = resources.stream()
                .filter(res -> res.getAnnotation(Api.Resource.class).value().equalsIgnoreCase(requestedResource))
                .findFirst().orElseThrow(() -> new ServerException(new NoSuchElementException()));



        //
        // Find method to invoke
        //
        Set<String> requestParams = Arrays.stream(request.getPath().split("/")).skip(2).collect(Collectors.toSet());
        Set<Method> methods = Arrays.stream(resourceClazz.getMethods()).filter(m -> m.isAnnotationPresent(Api.GET.class)).collect(Collectors.toSet());

        Method method = methods.stream()
                .filter(m -> Arrays.stream(m.getAnnotationsByType(Api.Param.class)).map(Api.Param::value).collect(Collectors.toSet()).equals(requestParams))
                .findFirst().orElse(null);

        try {
            Object instance = resourceClazz.getDeclaredConstructor().newInstance();
            method.invoke(instance, requestParams.toArray(new Object[0]));
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            throw new ServerException(e);
        }

        //TODO -- /solution
        throw new UnsupportedOperationException("Implement!");
    }

}
