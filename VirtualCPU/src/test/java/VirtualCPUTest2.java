import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class VirtualCPUTest2 {

	@Test(timeout=1000)
	public void test2() {
		CPUState state = new CPUState();
		VirtualCPU.LD.exec(state, 0, 75);
		VirtualCPU.MV.exec(state, 1, 0);
		VirtualCPU.MV.exec(state, 2, 0);
		VirtualCPU.MV.exec(state, 3, 0);
		VirtualCPU.ADD.exec(state, 0, 1);
		VirtualCPU.ADD.exec(state, 2, 3);

		boolean success = checkPort(state, 0, 0) & checkPort(state, 1, 0) & checkPort(state, 2, 0)
				& checkPort(state, 3, 0) & checkPort(state, 4, 0) & checkPort(state, 5, 0) & checkPort(state, 6, 0)
				& checkPort(state, 7, 0) & checkPort(state, 8, 0) & checkPort(state, 9, 0) & checkPort(state, 10, 0)
				& checkPort(state, 11, 0) & checkPort(state, 12, 0) & checkPort(state, 13, 0) & checkPort(state, 14, 0)
				& checkPort(state, 15, 0) & checkRegister(state, 0, 150) & checkRegister(state, 1, 75)
				& checkRegister(state, 2, 150) & checkRegister(state, 3, 75) & checkRegister(state, 4, 0)
				& checkRegister(state, 5, 0) & checkRegister(state, 6, 0) & checkRegister(state, 7, 0);
	}

	private boolean checkPort(CPUState state, int port, int value) {
		assertEquals("Port " + port + " heeft waarde " + state.getPort(port) + " terwijl " + value + " werd verwacht.", value, state.getPort(port));
		return true;
	}

	private boolean checkRegister(CPUState state, int register, int value) {
		assertEquals("Register " + register + " heeft waarde " + state.getRegister(register) + " terwijl "
				+ value + " werd verwacht.", value, state.getRegister(register));
		return true;
	}
}
